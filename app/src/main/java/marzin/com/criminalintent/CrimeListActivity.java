package marzin.com.criminalintent;

import android.support.v4.app.Fragment;

public class CrimeListActivity extends SingletonFragmentActivity {

    @Override
    protected Fragment createFragment(){
        return new CrimeListFragment();
    }

}