package marzin.com.criminalintent;


import android.support.v4.app.Fragment;

public class CrimeActivity extends SingletonFragmentActivity {

    @Override
    protected  Fragment createFragment(){
        return  new CrimeFragment();
    }

}
